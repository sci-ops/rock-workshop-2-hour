# Reproducible Open Coding Kit (ROCK) Workshop (2 hour version)

## Timetable

Intro & installation ---------------------------- 15:00 ---- 15:15
ROCK standard & definitions --------------------- 15:15 ---- 15:30
R, RStudio Cloud, R Markdown -------------------- 15:30 ---- 15:40
Data management for you and me ------------------ 15:40 ---- 15:45

Break ------------------------------------------- 15:45 ---- 16:00

Cleaning sources & identifying utterances ------- 16:00 ---- 16:15
Initial coding: iROCK, do you? ------------------ 16:15 ---- 16:30
Inspecting coded fragments ---------------------- 16:30 ---- 16:45
Epilogue: recoding, dichotomizing, exporting ---- 16:45 ---- 17:00

